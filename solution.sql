-- Create SQL Queries using advanced selects and joining tables to return the following:

	-- a. Find all artists that has letter D in its name.
	SELECT * FROM artists WHERE name LIKE "%_D%";
	SELECT * FROM artists WHERE name LIKE "%d%";

	-- b. Find all songs that has a length of less than 230.
	SELECT * FROM songs WHERE length < 230;

	-- c. Join the albums and songs tables (only album name, song name, and song length)
	SELECT album_title, song_name, length FROM albums
		JOIN songs ON albums.id = songs.album_id;

	-- d. Join the artists and albums tables (Find all albums that has letter A in its name)
	SELECT * FROM artists
		JOIN albums ON artists.id = albums.artist_id WHERE album_title LIKE "%_A%";

	SELECT * FROM artists
		JOIN albums ON artists.id = albums.artist_id;

	-- e. Sort the albums in Z-A order. (Show only 4 records)
	SELECT * FROM albums ORDER BY album_title DESC LIMIT 4;

	-- f. Join the albums and songs table. (Sort albums from Z-A and sort songs from A-Z)
		SELECT * FROM albums
 			JOIN songs ON albums.id = songs.album_id ORDER BY album_title DESC, song_name ASC;

